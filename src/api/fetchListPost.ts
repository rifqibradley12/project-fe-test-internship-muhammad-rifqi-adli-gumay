import axios from "axios";
import { PostModels } from "@/models/list-post";

export const fetchListPost = async (
  pageNumber: number,
  pageSize: number,
  sort: string,
): Promise<{ data: PostModels[]; total: number }> => {
  try {
    const response = await axios.get(
      `https://suitmedia-backend.suitdev.com/api/ideas`,
      {
        params: {
          "page[number]": pageNumber,
          "page[size]": pageSize,
          append: ["small_image", "medium_image"],
          sort,
        },
      },
    );

    if (response.status !== 200) {
      throw new Error(`An error has occurred: ${response.status}`);
    }

    return { data: response.data.data, total: response.data.meta.total };
  } catch (error) {
    console.error("Failed to fetch posts:", error);
    throw new Error("Failed to fetch posts");
  }
};
