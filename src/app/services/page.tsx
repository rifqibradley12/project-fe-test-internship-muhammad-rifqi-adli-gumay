"use client";

import Header from "@/components/Header";

export default function Services() {
  return (
    <div>
      <Header />
      <div className="flex justify-center items-center h-lvh flex-col bg-white text-black">
        <h1 className="font-bold text-2xl">This is Services Page</h1>
      </div>
    </div>
  );
}
