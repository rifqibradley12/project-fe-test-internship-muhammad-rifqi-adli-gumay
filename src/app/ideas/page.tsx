"use client";
import Header from "@/components/Header";
import Banner from "@/components/modules/banner/Banner";
import ListPost from "@/components/modules/list-post/ListPost";

export default function Ideas() {
  return (
    <div className="bg-white min-h-screen">
      <Header />
      <Banner />
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <ListPost />
      </div>
    </div>
  );
}
