"use client";

import Header from "@/components/Header";

export default function Contact() {
  return (
    <div>
      <Header />
      <div className="flex justify-center items-center h-lvh flex-col bg-white text-black">
        <h1 className="font-bold text-2xl">This is Contact Page</h1>
      </div>
    </div>
  );
}
