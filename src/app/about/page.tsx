"use client";

import Header from "@/components/Header";

export default function About() {
  return (
    <div>
      <Header />
      <div className="flex justify-center items-center h-lvh flex-col bg-white text-black">
        <h1 className="font-bold text-2xl">This is About Page</h1>
      </div>
    </div>
  );
}
