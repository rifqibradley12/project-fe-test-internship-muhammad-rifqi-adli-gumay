// types/list-post.type.ts
export interface PostModels {
  published_at: string | number | Date;
  id: number;
  title: string;
  small_image: Array<{
    id: number;
    mime: string;
    file_name: string;
    url: string;
  }>;
  medium_image: Array<{
    id: number;
    mime: string;
    file_name: string;
    url: string;
  }>;
}
