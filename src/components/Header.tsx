import { useState, useEffect } from "react";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { Assets } from "./constants";

export default function Header() {
  const [show, setShow] = useState(true);
  const [lastScrollY, setLastScrollY] = useState(0);
  const [isMenuOpen, setIsMenuOpen] = useState(false);

  const controlHeader = () => {
    if (typeof window !== "undefined") {
      if (window.scrollY > lastScrollY) {
        setShow(false);
      } else {
        setShow(true);
      }
      setLastScrollY(window.scrollY);
    }
  };

  useEffect(() => {
    if (typeof window !== "undefined") {
      window.addEventListener("scroll", controlHeader);
      return () => {
        window.removeEventListener("scroll", controlHeader);
      };
    }
  }, [lastScrollY]);

  const pathname = usePathname();

  return (
    <header
      className={`fixed top-0 left-0 w-full shadow transition-transform bg-[#eb6125] bg-opacity-90 duration-200 z-50 ${
        show ? "translate-y-0" : "-translate-y-full"
      }`}
    >
      <div className="flex justify-between items-center p-4 mx-auto max-w-screen-xl">
        <div className="flex-shrink-0">
          <Image src={Assets.logoSvg} alt="Logo" width={140} height={40} />
        </div>
        <button
          className="text-white lg:hidden"
          onClick={() => setIsMenuOpen(!isMenuOpen)}
        >
          {isMenuOpen ? "Close" : "Menu"}
        </button>
        <nav
          className={`flex-grow flex justify-end lg:static lg:flex lg:space-x-6 font-medium ${
            isMenuOpen ? "block" : "hidden"
          }`}
        >
          <ul className="flex flex-col lg:flex-row lg:space-x-6">
            {[
              { href: "/work", label: "Work" },
              { href: "/about", label: "About" },
              { href: "/services", label: "Services" },
              { href: "/ideas", label: "Ideas" },
              { href: "/careers", label: "Careers" },
              { href: "/contact", label: "Contact" },
            ].map((link) => (
              <li key={link.href} className="relative">
                <Link href={link.href}>
                  <span
                    className={`text-white hover:text-orange-300 cursor-pointer ${
                      pathname === link.href ? "border-b-2 border-white" : ""
                    }`}
                  >
                    {link.label}
                  </span>
                </Link>
              </li>
            ))}
          </ul>
        </nav>
      </div>
    </header>
  );
}
