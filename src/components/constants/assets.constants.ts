export class Assets {
  static readonly appLogo = "/assets/logo.png";
  static readonly logoSvg = "/assets/logo.svg";
  static readonly bannerImage = "/assets/banner.jpeg";
}
