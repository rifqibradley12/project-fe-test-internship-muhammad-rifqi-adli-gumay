import { useEffect, useRef } from "react";
import { Assets } from "@/components/constants";

const Banner = () => {
  const bannerRef = useRef<HTMLDivElement>(null);
  const textRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    const handleScroll = () => {
      const scrollTop = window.scrollY;
      if (bannerRef.current) {
        bannerRef.current.style.backgroundPositionY = `${scrollTop * 0.5}px`;
      }
      if (textRef.current) {
        textRef.current.style.transform = `translateY(${scrollTop * 0.2}px)`;
      }
    };

    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", handleScroll);
  }, []);

  return (
    <div className="relative h-[22rem] overflow-hidden">
      <div
        ref={bannerRef}
        className="absolute top-0 left-0 w-full h-full bg-cover bg-center"
        style={{ backgroundImage: `url(${Assets.bannerImage})` }}
      >
        <div
          ref={textRef}
          className="h-full flex flex-col items-center justify-center bg-black bg-opacity-60"
        >
          <h1 className="text-3xl md:text-5xl font-bold text-white">Ideas</h1>
          <p className="text-base md:text-lg font-light text-white mt-2">
            Where all our great things begin
          </p>
        </div>
      </div>
      <div
        className="absolute bottom-0 left-0 w-full h-20 bg-white"
        style={{ clipPath: "polygon(0 100%, 100% 0, 100% 100%)" }}
      ></div>
    </div>
  );
};

export default Banner;
