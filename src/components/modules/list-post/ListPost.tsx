import { useEffect, useState } from "react";
import { PostModels } from "@/models/list-post";
import { fetchListPost } from "@/api/fetchListPost";

const ListPost = () => {
  const [posts, setPosts] = useState<PostModels[]>([]);
  const [sort, setSort] = useState("-published_at");
  const [pageSize, setPageSize] = useState(10);
  const [pageNumber, setPageNumber] = useState(1);
  const [totalItems, setTotalItems] = useState(0);
  const [error, setError] = useState<string | null>(null);
  const totalPages = Math.ceil(totalItems / pageSize);

  const fetchPosts = async () => {
    try {
      const { data, total } = await fetchListPost(pageNumber, pageSize, sort);
      setPosts(data);
      setTotalItems(total);
      setError(null); // Clear any previous errors
    } catch (error) {
      console.error(error);
      setError("Failed to fetch posts");
    }
  };

  useEffect(() => {
    fetchPosts();
  }, [sort, pageSize, pageNumber]);

  const handlePrev = () => {
    setPageNumber(Math.max(1, pageNumber - 1));
  };

  const handleNext = () => {
    setPageNumber(Math.min(totalPages, pageNumber + 1));
  };

  const handleDoublePrev = () => {
    setPageNumber(Math.max(1, pageNumber - 5));
  };

  const handleDoubleNext = () => {
    setPageNumber(Math.min(totalPages, pageNumber + 5));
  };

  const renderPageButtons = () => {
    const pageButtons = [];
    const startPage = Math.floor((pageNumber - 1) / 5) * 5 + 1;
    const endPage = Math.min(startPage + 4, totalPages);

    for (let i = startPage; i <= endPage; i++) {
      pageButtons.push(
        <button
          key={i}
          onClick={() => setPageNumber(i)}
          className={`px-2 py-1 text-sm font-bold rounded-lg ${
            pageNumber === i
              ? "bg-orange-500 text-white"
              : "bg-white text-black hover:bg-gray-400"
          }`}
        >
          {i}
        </button>,
      );
    }
    return pageButtons;
  };

  return (
    <div className="bg-white p-6 mx-auto max-w-7xl">
      {error && <div className="text-red-500">{error}</div>}
      <div className="flex flex-col sm:flex-row justify-between items-center mb-4">
        <h1 className="font-bold text-black">
          Showing {posts.length} of {totalItems}
        </h1>
        <div className="flex flex-col sm:flex-row items-center space-x-4">
          <label className="flex items-center space-x-2 text-black">
            <span>Show per page:</span>
            <select
              onChange={(e) => setPageSize(Number(e.target.value))}
              value={pageSize}
              className="border border-gray-500 rounded-full py-2 px-4"
            >
              <option value={10}>10</option>
              <option value={20}>20</option>
              <option value={50}>50</option>
            </select>
          </label>
          <label className="flex items-center space-x-2 text-black">
            <span>Sort by:</span>
            <select
              onChange={(e) => setSort(e.target.value)}
              value={sort}
              className="border border-gray-500 rounded-full py-2 px-4"
            >
              <option value="-published_at">Newest</option>
              <option value="published_at">Oldest</option>
            </select>
          </label>
        </div>
      </div>
      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-4 gap-4">
        {posts.map((post) => (
          <div key={post.id} className="border p-2 rounded-lg shadow-lg">
            {post.small_image.length > 0 ? (
              <img
                src={post.small_image[0].url}
                alt={post.small_image[0].file_name}
                loading="lazy"
                className="w-full h-52 object-cover rounded-t-md"
              />
            ) : (
              <div className="w-full h-52 bg-gray-200 rounded-t-md flex items-center justify-center">
                <span className="text-gray-500">No Image</span>
              </div>
            )}
            <div className="p-2">
              <p className="text-gray-500 font-semibold text-sm my-1">
                {new Date(post.published_at).toLocaleDateString("id-ID", {
                  day: "numeric",
                  month: "long",
                  year: "numeric",
                })}
              </p>
              <h2 className="text-lg font-semibold line-clamp-3 text-black">
                {post.title}
              </h2>
            </div>
          </div>
        ))}
      </div>
      <div className="flex flex-col sm:flex-row justify-center my-10 space-x-2">
        <button
          onClick={handleDoublePrev}
          disabled={pageNumber <= 5}
          className="px-2 py-1 text-sm font-bold bg-white text-black rounded-lg hover:bg-gray-400 disabled:opacity-50"
        >
          ««
        </button>
        <button
          onClick={handlePrev}
          disabled={pageNumber === 1}
          className="px-2 py-1 text-sm font-bold bg-white text-black rounded-lg hover:bg-gray-400 disabled:opacity-50"
        >
          «
        </button>
        <div className="flex flex-wrap justify-center sm:justify-start">
          {renderPageButtons()}
        </div>
        <button
          onClick={handleNext}
          disabled={pageNumber === totalPages}
          className="px-2 py-1 text-sm font-bold bg-white text-black rounded-lg hover:bg-gray-400 disabled:opacity-50"
        >
          »
        </button>
        <button
          onClick={handleDoubleNext}
          disabled={pageNumber > totalPages - 5}
          className="px-2 py-1 text-sm font-bold bg-white text-black rounded-lg hover:bg-gray-400 disabled:opacity-50"
        >
          »»
        </button>
      </div>
    </div>
  );
};

export default ListPost;
