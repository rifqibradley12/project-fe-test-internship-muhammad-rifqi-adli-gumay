module.exports = {
  async redirects() {
    return [
      {
        source: "/",
        destination: "/ideas",
        permanent: true,
      },
    ];
  },
};
